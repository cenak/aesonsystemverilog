`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 25.12.2018 10:25:10
// Design Name: 
// Module Name: TestBench
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

//Message = HelloItIsTestBenchForAES
//Binary Message = 001100010100111001010101010101010101100000110010010111010011001001011100
//0011110101001110010111000101110100101011010011100101011101001100010100010
//01011110101100001011011001010100010111000111100
//
//Key = ItIsJustKeyForCW
//Binary Key = 001100100101110100110010010111000011001101011110010111000101110100110100
//01001110011000100010111101011000010110110010110001000000

module TestBench();
    logic _taskEnc; // 0 - encrypt; 1 - decrypt 
    logic [127:0] userKey;
    logic [191:0] inputTextEnc;
    logic [191:0] outputTextEnc;
	
	logic _taskDec;
	logic [191:0] inputTextDec;
    logic [191:0] outputTextDec;

    top TopEnc(_taskEnc, userKey, inputTextEnc, outputTextEnc);
	
	top TopDec(_taskDec, userKey, inputTextDec, outputTextDec);
	
    
    //RoundKeyGenerator rkg(userKey, zeroRoundKey, firstRoundKey, secondRoundKey, thirdRoundKey, fourthRoundKey, fifthRoundKey, sixthRoundKey,
    //                        seventhRoundKey, eighthRoundKey, ninthRoundKey, tenthRoundKey, allKeysGenerated);
    
    //Encoder enc(inputText, zeroRoundKey, firstRoundKey, secondRoundKey, thirdRoundKey, fourthRoundKey, fifthRoundKey, sixthRoundKey,
    //            seventhRoundKey, eighthRoundKey, ninthRoundKey, tenthRoundKey, ciphertext, allKeysGenerated, finish, tst, tst2);
	
	

	
	//logic [191:0] tst;
	//Decoder dec(inputTextDec, zeroRoundKey, firstRoundKey, secondRoundKey, thirdRoundKey, fourthRoundKey, fifthRoundKey, sixthRoundKey,
    //            seventhRoundKey, eighthRoundKey, ninthRoundKey, tenthRoundKey, outputTextDec, allKeysGenerated, finish, tst);
	
	
    initial
    begin
	
        _taskEnc = 1'b0;
		
		_taskDec = 1'b1;
    
        userKey = 128'b00110010010111010011001001011100001100110101111001011100010111010011010001001110011000100010111101011000010110110010110001000000;
        
        inputTextEnc = 192'b001100010100111001010101010101010101100000110010010111010011001001011100001111010100111001011100010111010010101101001110010101110100110001010001001011110101100001011011001010100010111000111100;
		
		inputTextDec = 192'h8431bcb3cdf73c106aa651f36c07623ef93ac1a72bf7082a;
        
        //$display("Running testbench");
        //$display("_task = %d", _task);
        //$display("userKey = %d", userKey);
        //$display("inputText = %d", inputText);
        //$display("outputText = %d", outputText);
    end

endmodule
