`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08.12.2018 13:29:11
// Design Name: 
// Module Name: RoundKeyGenerator
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module RoundKeyGenerator(
    input logic [127:0] userKey,
    output logic [127:0] zeroRoundKey,
    output logic [127:0] firstRoundKey,
    output logic [127:0] secondRoundKey,
    output logic [127:0] thirdRoundKey,
    output logic [127:0] fourthRoundKey,
    output logic [127:0] fifthRoundKey,
    output logic [127:0] sixthRoundKey,
    output logic [127:0] seventhRoundKey,
    output logic [127:0] eighthRoundKey,
    output logic [127:0] ninthRoundKey,
    output logic [127:0] tenthRoundKey,
    output logic allKeysGenerated
    );
    
    //-----------------Temp Wires---------------------------
    logic [127:0] _firstRoundKey;
    logic [127:0] _secondRoundKey;
    logic [127:0] _thirdRoundKey;
    logic [127:0] _fourthRoundKey;
    logic [127:0] _fifthRoundKey;
    logic [127:0] _sixthRoundKey;
    logic [127:0] _seventhRoundKey;
    logic [127:0] _eighthRoundKey;
    logic [127:0] _ninthRoundKey;
    logic [127:0] _tenthRoundKey;
    
    logic [31:0] t4;
    logic [31:0] t8;
    logic [31:0] t12;
    logic [31:0] t16;
    logic [31:0] t20;
    logic [31:0] t24;
    logic [31:0] t28;
    logic [31:0] t32;
    logic [31:0] t36;
    logic [31:0] t40;
    
    SubWord subWord1(tVar1[31:24], tVar2[31:24]);
    SubWord subWord2(tVar1[23:16], tVar2[23:16]);
    SubWord subWord3(tVar1[15:8], tVar2[15:8]);
    SubWord subWord4(tVar1[7:0], tVar2[7:0]);
    
    //-----------------Generated temp variable--------------
    typedef enum {
       ZERO, FIRST, SECOND, THIRD, FOURTH, FIFTH, SIXTH, SEVENTH, EIGHTH, NINTH, TENTH, FINISH
    } SignalThatRoundKeyWasGenerated;
    
    typedef enum {
       zero, first, second, third, fourth, fifth, sixth, seventh, eighth, ninth, tenth, finish
    } SignalThatTempVarWasGenerated;
    
    SignalThatRoundKeyWasGenerated signalThatRoundKeyWasGenerated = ZERO;
    SignalThatTempVarWasGenerated signalThatTempVarWasGenerated = zero;
    
    
    
    
    //-----------------Temp variable generator---------------
    
    logic [31:0] tVar1;
    logic [31:0] tVar2;
    logic [31:0] tVar3;
    
    always_comb
    begin
        case (signalThatRoundKeyWasGenerated)
            ZERO:   begin
                    tVar1 = userKey[31:0];
                    tVar1 = tVar1 <<< 8;
                    #1;
                    tVar3 = tVar2;
                    tVar3 = tVar3 ^ 32'h01000000;
                    t4 = tVar3;
					
                    signalThatTempVarWasGenerated = first;
                    end
                    
            FIRST:  begin
                    tVar1 = _firstRoundKey[31:0];
                    tVar1 = tVar1 <<< 8;
                    #1;
                    tVar3 = tVar2;
                    tVar3 = tVar3 ^ 32'h02000000;
                    t8 = tVar3;
					
                    signalThatTempVarWasGenerated = second;
                    end
                    
            SECOND: begin
					tVar1 = _secondRoundKey[31:0];
                    tVar1 = tVar1 <<< 8;
                    #1;
                    tVar3 = tVar2;
                    tVar3 = tVar3 ^ 32'h04000000;
                    t12 = tVar3;
					
                    signalThatTempVarWasGenerated = third;
                    end
                    
            THIRD:  begin
					tVar1 = _thirdRoundKey[31:0];
                    tVar1 = tVar1 <<< 8;
                    #1;
                    tVar3 = tVar2;
                    tVar3 = tVar3 ^ 32'h08000000;
                    t16 = tVar3;
					
                    signalThatTempVarWasGenerated = fourth;
                    end
                    
            FOURTH: begin
                    tVar1 = _fourthRoundKey[31:0];
                    tVar1 = tVar1 <<< 8;
                    #1;
                    tVar3 = tVar2;
                    tVar3 = tVar3 ^ 32'h10000000;
                    t20 = tVar3;
					
                    signalThatTempVarWasGenerated = fifth;
                    end
                    
            FIFTH:  begin
					tVar1 = _fifthRoundKey[31:0];
                    tVar1 = tVar1 <<< 8;
                    #1;
                    tVar3 = tVar2;
                    tVar3 = tVar3 ^ 32'h20000000;
                    t24 = tVar3;
					
                    signalThatTempVarWasGenerated = sixth;
                    end
                    
            SIXTH:  begin
					tVar1 = _sixthRoundKey[31:0];
                    tVar1 = tVar1 <<< 8;
                    #1;
                    tVar3 = tVar2;
                    tVar3 = tVar3 ^ 32'h40000000;
                    t28 = tVar3;
					
                    signalThatTempVarWasGenerated = seventh;
                    end
                    
            SEVENTH:begin
					tVar1 = _seventhRoundKey[31:0];
                    tVar1 = tVar1 <<< 8;
                    #1;
                    tVar3 = tVar2;
                    tVar3 = tVar3 ^ 32'h80000000;
                    t32 = tVar3;
					
                    signalThatTempVarWasGenerated = eighth;
                    end
                    
            EIGHTH: begin
					tVar1 = _eighthRoundKey[31:0];
                    tVar1 = tVar1 <<< 8;
                    #1;
                    tVar3 = tVar2;
                    tVar3 = tVar3 ^ 32'h1B000000;
                    t36 = tVar3;
					
                    signalThatTempVarWasGenerated = ninth;
                    end
                    
            NINTH:  begin
					tVar1 = _ninthRoundKey[31:0];
                    tVar1 = tVar1 <<< 8;
                    #1;
                    tVar3 = tVar2;
                    tVar3 = tVar3 ^ 32'h36000000;
                    t40 = tVar3;
					
                    signalThatTempVarWasGenerated = tenth;
                    end
                    
            TENTH:  begin
                    
                    end
					
			default: ;
        endcase
        
    end
    
    //---------------------Round key generator--------------------
    always_comb
    begin
        case (signalThatTempVarWasGenerated)
        
            first:  begin
                    _firstRoundKey[127:96] = t4 ^ userKey[127:96];
                    _firstRoundKey[95:64] = _firstRoundKey[127:96] ^ userKey[95:64];
                    _firstRoundKey[63:32] = _firstRoundKey[95:64] ^ userKey[63:32];
                    _firstRoundKey[31:0] = _firstRoundKey[63:32] ^ userKey[31:0];
                    
                    signalThatRoundKeyWasGenerated = FIRST;
                    end
                    
            second: begin
                    _secondRoundKey[127:96] = t8 ^ _firstRoundKey[127:96];
                    _secondRoundKey[95:64] = _secondRoundKey[127:96] ^ _firstRoundKey[95:64];
                    _secondRoundKey[63:32] = _secondRoundKey[95:64] ^ _firstRoundKey[63:32];
                    _secondRoundKey[31:0] = _secondRoundKey[63:32] ^ _firstRoundKey[31:0];
                    
                    signalThatRoundKeyWasGenerated = SECOND;
                    end
                    
            third:  begin
					_thirdRoundKey[127:96] = t12 ^ _secondRoundKey[127:96];
                    _thirdRoundKey[95:64] = _thirdRoundKey[127:96] ^ _secondRoundKey[95:64];
                    _thirdRoundKey[63:32] = _thirdRoundKey[95:64] ^ _secondRoundKey[63:32];
                    _thirdRoundKey[31:0] = _thirdRoundKey[63:32] ^ _secondRoundKey[31:0];
                    
                    signalThatRoundKeyWasGenerated = THIRD;
                    end
                    
            fourth:	begin
                    _fourthRoundKey[127:96] = t16 ^ _thirdRoundKey[127:96];
                    _fourthRoundKey[95:64] = _fourthRoundKey[127:96] ^ _thirdRoundKey[95:64];
                    _fourthRoundKey[63:32] = _fourthRoundKey[95:64] ^ _thirdRoundKey[63:32];
                    _fourthRoundKey[31:0] = _fourthRoundKey[63:32] ^ _thirdRoundKey[31:0];
                    
                    signalThatRoundKeyWasGenerated = FOURTH;
                    end
                    
            fifth:  begin
					_fifthRoundKey[127:96] = t20 ^ _fourthRoundKey[127:96];
                    _fifthRoundKey[95:64] = _fifthRoundKey[127:96] ^ _fourthRoundKey[95:64];
                    _fifthRoundKey[63:32] = _fifthRoundKey[95:64] ^ _fourthRoundKey[63:32];
                    _fifthRoundKey[31:0] = _fifthRoundKey[63:32] ^ _fourthRoundKey[31:0];
                    
                    signalThatRoundKeyWasGenerated = FIFTH;
                    end
                    
            sixth:  begin
					_sixthRoundKey[127:96] = t24 ^ _fifthRoundKey[127:96];
                    _sixthRoundKey[95:64] = _sixthRoundKey[127:96] ^ _fifthRoundKey[95:64];
                    _sixthRoundKey[63:32] = _sixthRoundKey[95:64] ^ _fifthRoundKey[63:32];
                    _sixthRoundKey[31:0] = _sixthRoundKey[63:32] ^ _fifthRoundKey[31:0];
                    
                    signalThatRoundKeyWasGenerated = SIXTH;
                    end
                    
            seventh:begin
					_seventhRoundKey[127:96] = t28 ^ _sixthRoundKey[127:96];
                    _seventhRoundKey[95:64] = _seventhRoundKey[127:96] ^ _sixthRoundKey[95:64];
                    _seventhRoundKey[63:32] = _seventhRoundKey[95:64] ^ _sixthRoundKey[63:32];
                    _seventhRoundKey[31:0] = _seventhRoundKey[63:32] ^ _sixthRoundKey[31:0];
                    
                    signalThatRoundKeyWasGenerated = SEVENTH;
                    end
                    
            eighth: begin
					_eighthRoundKey[127:96] = t32 ^ _seventhRoundKey[127:96];
                    _eighthRoundKey[95:64] = _eighthRoundKey[127:96] ^ _seventhRoundKey[95:64];
                    _eighthRoundKey[63:32] = _eighthRoundKey[95:64] ^ _seventhRoundKey[63:32];
                    _eighthRoundKey[31:0] = _eighthRoundKey[63:32] ^ _seventhRoundKey[31:0];
                    
                    signalThatRoundKeyWasGenerated = EIGHTH;
                    end
                    
            ninth:  begin
					_ninthRoundKey[127:96] = t36 ^ _eighthRoundKey[127:96];
                    _ninthRoundKey[95:64] = _ninthRoundKey[127:96] ^ _eighthRoundKey[95:64];
                    _ninthRoundKey[63:32] = _ninthRoundKey[95:64] ^ _eighthRoundKey[63:32];
                    _ninthRoundKey[31:0] = _ninthRoundKey[63:32] ^ _eighthRoundKey[31:0];
                    
                    signalThatRoundKeyWasGenerated = NINTH;
                    end
                    
            tenth:  begin
                    _tenthRoundKey[127:96] = t40 ^ _ninthRoundKey[127:96];
                    _tenthRoundKey[95:64] = _tenthRoundKey[127:96] ^ _ninthRoundKey[95:64];
                    _tenthRoundKey[63:32] = _tenthRoundKey[95:64] ^ _ninthRoundKey[63:32];
                    _tenthRoundKey[31:0] = _tenthRoundKey[63:32] ^ _ninthRoundKey[31:0];
                    
                    signalThatRoundKeyWasGenerated = FINISH;
                    allKeysGenerated = 1;
                    end
                    
            default: ;
        
        endcase
    end
    
    assign zeroRoundKey = userKey;
    assign firstRoundKey = _firstRoundKey;
    assign secondRoundKey = _secondRoundKey;
    assign thirdRoundKey = _thirdRoundKey;
    assign fourthRoundKey = _fourthRoundKey;
    assign fifthRoundKey = _fifthRoundKey;
    assign sixthRoundKey = _sixthRoundKey;
    assign seventhRoundKey = _seventhRoundKey;
    assign eighthRoundKey = _eighthRoundKey;
    assign ninthRoundKey = _ninthRoundKey;
    assign tenthRoundKey = _tenthRoundKey;
    
endmodule
