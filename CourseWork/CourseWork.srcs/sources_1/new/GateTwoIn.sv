`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02.12.2018 17:30:32
// Design Name: 
// Module Name: GateTwoIn
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module GateTwoIn(
    //input logic clk,
    input logic direction,
    input logic [191:0] encWire,
    input logic [191:0] decWire,
    output logic [191:0] outWire,
    input logic finish
    );
    
    logic [191:0] tWire;
    
    always_comb
    begin
        if(finish)
        begin
            if(!direction)
                tWire = encWire;
                
            else
                tWire = decWire;
            
            outWire = tWire;
        end
    end
    
    //assign outWire = tWire;
        
    
endmodule
