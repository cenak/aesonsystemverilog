`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02.12.2018 17:20:34
// Design Name: 
// Module Name: top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

/*
* 10 rounds will used, because key's length 128 bits 
*
*
*
*
*
*
*/

module top(
    //input logic clk,
    input logic _task, // 0 - encrypt; 1 - decrypt 
    input logic [127:0] userKey,
    input logic [191:0] inputText, //plaintext
    output logic [191:0] outputText //ciphertext
    );
    
    logic [191:0] wireToEncrypt;
    logic [191:0] wireFromEncrypt;
    logic [191:0] wireToDecrypt;
    logic [191:0] wireFromDecrypt;
    
    logic [127:0] zeroRoundKey;
    logic [127:0] firstRoundKey;
    logic [127:0] secondRoundKey;
    logic [127:0] thirdRoundKey;
    logic [127:0] fouthRoundKey;
    logic [127:0] fifthRoundKey;
    logic [127:0] sixthRoundKey;
    logic [127:0] seventhRoundKey;
    logic [127:0] eighthtRoundKey;
    logic [127:0] ninthRoundKey;
    logic [127:0] tenthRoundKey;
    
    logic allKeysGenerated;
    logic finishD;
    logic finishE;
    logic finishOr;
    
    assign finishOr = finishD | finishE;
    
    //Gates
    GateTwoOut gateChooseMode(_task, inputText, wireToEncrypt, wireToDecrypt);
    GateTwoIn gateResult(_task, wireFromEncrypt, wireFromDecrypt, outputText, finishOr);
    
    //Encoder and Decoder
    Encoder encoder(wireToEncrypt, zeroRoundKey, firstRoundKey, secondRoundKey,
                    thirdRoundKey, fouthRoundKey, fifthRoundKey, sixthRoundKey,
                    seventhRoundKey, eighthtRoundKey, ninthRoundKey, tenthRoundKey, wireFromEncrypt, allKeysGenerated, finishE);
                                            
    Decoder decoder(wireToDecrypt, zeroRoundKey, firstRoundKey, secondRoundKey,
                    thirdRoundKey, fouthRoundKey, fifthRoundKey, sixthRoundKey,
                    seventhRoundKey, eighthtRoundKey, ninthRoundKey, tenthRoundKey, wireFromDecrypt, allKeysGenerated, finishD);
    
    //Key Generator
    RoundKeyGenerator roundKeyGenerator(userKey, zeroRoundKey, firstRoundKey, secondRoundKey,
                                        thirdRoundKey, fouthRoundKey, fifthRoundKey, sixthRoundKey,
                                        seventhRoundKey, eighthtRoundKey, ninthRoundKey, tenthRoundKey, allKeysGenerated);
    
endmodule
