`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02.12.2018 17:37:20
// Design Name: 
// Module Name: GateTwoOut
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module GateTwoOut(
    //input logic clk,
    input logic direction,
    input logic [191:0] inWire,
    output logic [191:0] outEncWire,
    output logic [191:0] outDecWire
    );

    logic [191:0] tWire;

    assign tWire = inWire;

    always_comb
    begin
        if(direction == 0)
            outEncWire = tWire;
        
        else
            outDecWire = tWire;
    end    

endmodule
